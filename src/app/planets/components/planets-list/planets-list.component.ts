import { Component, OnInit, OnDestroy } from '@angular/core';
import { PlanetContainer } from '../../models/planet.model';
import { PlanetsService } from '../../services/planets.service';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-planets-list',
  templateUrl: './planets-list.component.html',
  styleUrls: ['./planets-list.component.scss']
})
export class PlanetsListComponent implements OnInit, OnDestroy {

  planets: PlanetContainer;
  sub: Subscription;

  constructor(private planetsService: PlanetsService) { }

  ngOnInit() {
    this.getPlanets(1);
  }

  getPlanets(page) {
    this.sub = this.planetsService.getPlanets(page).subscribe(
      res => this.planets = res
    );
  }

  paginate(event) {
    this.getPlanets(event.page + 1);

    const scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset;
      if (pos > 0) {
          window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
          window.clearInterval(scrollToTop);
      }
  }, 16);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
