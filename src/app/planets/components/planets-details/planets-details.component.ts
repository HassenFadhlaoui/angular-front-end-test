import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlanetsService } from '../../services/planets.service';
import { Planet } from '../../models/planet.model';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';

import {PaginatorModule} from 'primeng/paginator';

@Component({
  selector: 'app-planets-details',
  templateUrl: './planets-details.component.html',
  styleUrls: ['./planets-details.component.scss']
})
export class PlanetsDetailsComponent implements OnInit, OnDestroy {

  id;
  planet: Planet;
  sub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private planetsService: PlanetsService,
    private location: Location) { }

  ngOnInit() {

    this.id = this.route.snapshot.paramMap.get('id');

    this.sub = this.planetsService.getPlanetById(this.id).subscribe(
      res => this.planet = res
    );

  }

  goBack() {
    // this._location.back();
    this.router.navigate(['/planets']);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
