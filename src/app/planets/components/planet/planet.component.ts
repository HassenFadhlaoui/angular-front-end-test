import { Component, OnInit, Input } from '@angular/core';
import { Planet } from '../../models/planet.model';

@Component({
  selector: 'app-planet',
  templateUrl: './planet.component.html',
  styleUrls: ['./planet.component.scss']
})
export class PlanetComponent implements OnInit {

  @Input() planet: Planet;
  @Input() showArrow: boolean;

  planetId;

  constructor() { }

  ngOnInit() {
    if (this.planet && this.planet.url) {
      this.planetId = Number( this.planet.url.match(/\d+/g).map(Number) );
    }

  }

}
