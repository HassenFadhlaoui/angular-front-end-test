import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanetsListComponent } from './components/planets-list/planets-list.component';
import { PlanetsDetailsComponent } from './components/planets-details/planets-details.component';


const routes: Routes = [
  {
    path: '',
    component: PlanetsListComponent
  },
  {
    path: ':id',
    component: PlanetsDetailsComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanetsRoutingModule { }
