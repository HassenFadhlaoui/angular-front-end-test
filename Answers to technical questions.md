# How long did you spend on the coding test?
- about 4 hours
# What would you add to your solution if you had more time?
- Animation
# How would you improve the app you just made ?
- add unit tests
- add an interceptor for htpp request and handele both errors and responses
- prepare the app to expand : adding a shared folder, add a new module called modules for all the rest of the modules(look for an app architecture that fit to the app)...
- add caching service to minimize http requests
