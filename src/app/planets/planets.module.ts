import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanetsRoutingModule } from './planets-routing.module';
import { PlanetComponent } from './components/planet/planet.component';
import { PlanetsListComponent } from './components/planets-list/planets-list.component';
import { PlanetsDetailsComponent } from './components/planets-details/planets-details.component';
// primeng modules
import { PaginatorModule } from 'primeng/components/paginator/paginator';


@NgModule({
  declarations: [PlanetComponent, PlanetsListComponent, PlanetsDetailsComponent],
  imports: [
    CommonModule,
    PlanetsRoutingModule,
    PaginatorModule
  ]
})
export class PlanetsModule { }
