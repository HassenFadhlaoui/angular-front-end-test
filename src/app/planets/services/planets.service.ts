import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PlanetContainer, Planet } from '../models/planet.model';

@Injectable({
  providedIn: 'root'
})
export class PlanetsService {

  apiRoot = 'https://swapi.co/api';

  constructor(private http: HttpClient) { }

  getPlanets(page): Observable<PlanetContainer> {
    return this.http.get<PlanetContainer>(`${this.apiRoot}/planets?page=${page}`);
  }

  getPlanetById(id): Observable<Planet> {
    return this.http.get<Planet>(`${this.apiRoot}/planets/${id}`);
  }

}
